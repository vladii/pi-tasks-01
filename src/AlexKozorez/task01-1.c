#include <stdio.h>
#include <math.h>

int main(){
	
	int rubles = -1, subRubles = -1;
	
	while(rubles<0 || subRubles<0){
		printf("\n Please, enter the price in the format RR.SS \n");
		printf(" Example: 15.20 \n ");
		scanf("%d.%d", &rubles, &subRubles);
	}

	subRubles > 99 ? printf(" %d rubles %d subRubles", rubles+(subRubles/100), subRubles%100) : printf(" %d rubles %d subRubles", rubles, subRubles);

	return 0;	
}