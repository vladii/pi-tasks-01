#define STRING_LENGTH 256

#include <stdio.h>
#include <string.h>

int main() 
{
	int i = 0, SpaceFlag = 0;
	char string[STRING_LENGTH];
	printf("Enter a string of numbers: ");
	fgets(string,STRING_LENGTH,stdin);
	printf("Converted string: ");
	string[strlen(string) - 1] = 0;
	SpaceFlag = strlen(string) % 3;
	if (SpaceFlag == 0)
	{
		printf("%c", string[i]);
		i++;
	}
	while (string[i])
	{
		if (i%3 != SpaceFlag)
		{
			printf("%c", string[i]);
		}
		else
		{
			printf(" %c", string[i]);
			SpaceFlag == 0;
		}
		i++;
	}
	printf("\n");
	return 0;
}