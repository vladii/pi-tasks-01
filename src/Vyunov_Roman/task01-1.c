#define _CRT_SECURE_NO_WARNINGS
#define RUB_IN_DIME (100)

#include <stdio.h>

int main() 
{
	unsigned int rubles = 0, dime = 0;
	
	printf("Enter sum as rubles.dime : ");
	scanf("%d.%d",&rubles,&dime);
	if (dime>=RUB_IN_DIME)
	{
		rubles += dime / RUB_IN_DIME;
		dime %= RUB_IN_DIME;
	}
	printf("%d rub. %d dime\n",rubles,dime);
	
	return 0;
}