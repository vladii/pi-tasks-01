#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	int rub = 0, kop = 0;
	printf("Enter sum in the format rub.kop\n");
	scanf("%d.%d", &rub, &kop);
	if (rub < 0 || kop < 0)
	{
		puts("Input error!");
		return 1;
	}
	if (kop > 99)
	{
		rub += kop / 100;
		kop %= 100;
	}
	printf("%d rub. %d kop.", rub, kop);
	return 0;
}