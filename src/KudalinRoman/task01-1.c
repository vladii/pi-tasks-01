#define _CRT_SECURE_NO_WARNINGS
#include <locale.h>
#include <stdio.h>
int main()
{	
	setlocale(LC_ALL, "RUS");
	int val1=-1, val2=-1;
	puts("Enter a string in the following form: rr.kk");
	scanf("%d.%d", &val1, &val2);
	if (val1 <0 || val2 < 0)
	{
		puts("Input Error!");
		return 1;
	}
	if (val2 > 99)
	{
		val1+=val2/100;
		val2%=100;
	}
	printf("%d ���. %d ���.\n", val1, val2);
	return 0;
}