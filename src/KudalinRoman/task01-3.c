#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	char str[256];
	int i=0, number=0, multiply=1, sum=0;
	puts("Enter a string, please");
	fgets(str,256,stdin);
	while (str[i])
	{
		if (str[i] >= '0' && str[i] <= '9')
		{
			number = number*multiply + (str[i]-'0');
			multiply=10;
			i++;
		}
		else if (str[i]<'0' || str[i]>'9')
		{	
			sum +=number;
			number=0;
			multiply=1;
			i++;
		}
	}
	printf("The sum of numbers: %d\n", sum);
	return 0;
}