#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

int main()
{
    int sum = 0;
    char buf[256];
    int i = 0;
    printf("Enter a line, please: \n");
    fgets(buf, 256, stdin);
    while (buf[i])
    {
        if (buf[i] >= '0' && buf[i] <= '9')
        {
            //sum += buf[i] - '0';
            sum += (atoi(buf + i));
            while (buf[i] >= '0' && buf[i] <= '9')
                i++;
        }
        i++;
    }
    printf("sum=%d \n", sum);
        return 0;
}		